## Live coding interview
### Technical requirements
- Repository: [link](https://gitlab.com/radamek_wh/codingtask/-/tree/main)
- Android Studio v. Chipmunk: [link](https://developer.android.com/studio)
- Android API 32
- Kotlin version 1.6.21
- Java version 1.8
- You are going to share your screen during the interview and the code needs to be visible. Suggestions:
  - Use Presentation Mode in Android Studio: [link](https://www.jetbrains.com/help/idea/ide-viewing-modes.html)
  - Increase the font size of Android Studio
  - Decrease your screen resolution in case it is too high

### Task
Show the details of each user on an individual screen.
You will need to:
- Add new endpoint to the '/data/remote/**UsersApi**' to request the details of a single user.
  - Example response: [link](https://gitlab.com/-/snippets/2357431/raw/main/1.json)
  - Generic endpoint: `https://gitlab.com/-/snippets/2357431/raw/main/{userId}.json`
- Add new model '/data/remote/model/**UserDetailsModel**' to parse the API response.
- Create '/ui/view/userDetails/**UserDetailsViewModel**' that fetches the data using /data/UsersRepository and publishes it for the Fragment to consume.
- Create Fragment '/ui/view/userDetails/**UserDetailsFragment**'.
- Create layout using Jetpack Compose. Suggested UI:

<img src="app/src/main/res/drawable/detail_view_example.jpg" alt="Details view example" style="height:245.87px; width:113.5px;"/>


### Task goals
#### Required
- Show confidence with the code written and decisions taken
- Understand the technologies used
#### Desired
- Follow MVI architecture
- Use the technologies requested, i.e.: Build the UI using Jetpack Compose
- Complete the task
- Create unit tests
