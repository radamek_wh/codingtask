package com.williamhill.gamingusers.data

import com.williamhill.gamingusers.data.remote.ResponseResult
import com.williamhill.gamingusers.data.remote.UsersApi
import com.williamhill.gamingusers.data.remote.model.UserListItemModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val usersApi: UsersApi,
) : UsersRepository {

    override suspend fun users(): Flow<ResponseResult<List<UserListItemModel>>> = flow {
        with(usersApi.users()) {
            val responseBody = body()
            if (isSuccessful && responseBody != null) {
                emit(ResponseResult.Success(responseBody))
            } else {
                emit(ResponseResult.Error(message()))
            }
        }
    }

}
