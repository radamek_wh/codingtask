package com.williamhill.gamingusers.data.remote

import com.williamhill.gamingusers.data.remote.model.UserListItemModel
import retrofit2.Response
import retrofit2.http.GET

interface UsersApi {

    @GET("users.json")
    suspend fun users(): Response<List<UserListItemModel>>

}
