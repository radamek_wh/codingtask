package com.williamhill.gamingusers.data.remote.model

import com.squareup.moshi.Json

data class UserModel(
    @Json(name = "_id")
    val id: String,
    val about: String,
    val address: String,
    val age: Int,
    val balance: String,
    val company: String,
    val email: String,
    val eyeColor: String,
    val gender: String,
    val isActive: Boolean,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phone: String,
    val picture: String,
    val registered: String,
    val tags: List<String>
)
