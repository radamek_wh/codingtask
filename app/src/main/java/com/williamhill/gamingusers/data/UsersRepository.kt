package com.williamhill.gamingusers.data

import com.williamhill.gamingusers.data.remote.ResponseResult
import com.williamhill.gamingusers.data.remote.model.UserListItemModel
import kotlinx.coroutines.flow.Flow

interface UsersRepository {
    suspend fun users(): Flow<ResponseResult<List<UserListItemModel>>>
}
