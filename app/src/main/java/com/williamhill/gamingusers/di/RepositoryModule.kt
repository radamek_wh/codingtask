package com.williamhill.gamingusers.di

import com.williamhill.gamingusers.data.UsersRepository
import com.williamhill.gamingusers.data.UsersRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideUsersRepository(usersRepository: UsersRepositoryImpl): UsersRepository =
        usersRepository

}
