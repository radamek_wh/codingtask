package com.williamhill.gamingusers.ui.view.users

import com.williamhill.gamingusers.core.base.BaseViewModel
import com.williamhill.gamingusers.data.UsersRepository
import com.williamhill.gamingusers.data.remote.ResponseResult
import com.williamhill.gamingusers.data.remote.model.UserListItemModel
import com.williamhill.gamingusers.ui.view.users.contract.UsersIntent
import com.williamhill.gamingusers.ui.view.users.contract.UsersState
import com.williamhill.gamingusers.ui.view.users.contract.UsersViewEvent
import com.williamhill.gamingusers.ui.view.users.model.UserItem
import com.williamhill.gamingusers.ui.view.users.model.toUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@FlowPreview
@HiltViewModel
class UsersViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
) : BaseViewModel<UsersState, UsersIntent, UsersViewEvent>() {

    /** Initial state to be rendered by the view */
    override val initialState: UsersState
        get() = UsersState()

    /**
     * Handles [UsersIntent] sent from the view
     * and returns a flow with the reduced state
     */
    override fun handleIntent(state: UsersState, intent: UsersIntent): Flow<UsersState> {
        val usersFlow = flowOf(intent)
            .filterIsInstance<UsersIntent.LoadUsers>()
            .flatMapConcat {
                usersRepository
                    .users()
                    .map {
                        reduceUsersResponseToUsersState(
                            currentState = state,
                            response = it,
                        )
                    }
                    .onStart {
                        emit(
                            state.copy(
                                users = arrayListOf<UserItem.Skeleton>().apply {
                                    for (i in 0..20) add(UserItem.Skeleton)
                                }
                            )
                        )
                    }
            }

        val navigateToUserFlow = flowOf(intent)
            .filterIsInstance<UsersIntent.NavigateToUser>()
            .flatMapConcat { flowOf(state) }

        return merge(
            usersFlow,
            navigateToUserFlow,
        )
    }

    /**
     * Handles [UsersIntent] sent from the view and returns a flow of
     * [UsersViewEvent] that is expected to produce a single event in the view
     */
    override fun handleViewEvent(intent: UsersIntent): UsersViewEvent? =
        when(intent) {
            is UsersIntent.NavigateToUser -> UsersViewEvent.NavigateToUser(intent.id)
            else -> null
        }

    private fun reduceUsersResponseToUsersState(
        currentState: UsersState,
        response: ResponseResult<List<UserListItemModel>>,
    ): UsersState =
        when (response) {
            is ResponseResult.Success ->
                    currentState.copy(users = response.data.map { user -> user.toUser() })
            is ResponseResult.Error ->
                    currentState.copy(errorMessage = response.message)
        }

}