package com.williamhill.gamingusers.ui

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.williamhill.gamingusers.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GamingUsersActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gaming_users)
    }

}
