package com.williamhill.gamingusers.ui.view.users

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.williamhill.gamingusers.ui.custom.loadingShimmerEffect

@Composable
fun UserLoadingView(
    modifier: Modifier = Modifier,
    brush: Brush = loadingShimmerEffect(2500),
) {

    Surface(
        tonalElevation = 1.dp,
        shadowElevation = 0.dp,
        shape = MaterialTheme.shapes.small,
        modifier = modifier.fillMaxWidth(),
    ) {
        Column(
            modifier = modifier.padding(16.dp),
        ) {
            // Headline
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Spacer(modifier = Modifier
                    .width(260.dp)
                    .height(40.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
                Spacer(modifier = Modifier
                    .width(40.dp)
                    .height(40.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
            }
            Spacer(modifier = Modifier.size(8.dp))

            // Balance
            Spacer(modifier = Modifier
                .width(100.dp)
                .height(32.dp)
                .background(brush = brush, shape = RoundedCornerShape(4.dp)))
            Spacer(modifier = Modifier.size(8.dp))

            // Email
            Row {
                Spacer(modifier = Modifier
                    .width(40.dp)
                    .height(24.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
                Spacer(modifier = Modifier.size(16.dp))
                Spacer(modifier = Modifier
                    .width(180.dp)
                    .height(24.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
            }
            Spacer(modifier = Modifier.size(8.dp))

            // Phone
            Row {
                Spacer(modifier = Modifier
                    .width(40.dp)
                    .height(24.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
                Spacer(modifier = Modifier.size(16.dp))
                Spacer(modifier = Modifier
                    .width(180.dp)
                    .height(24.dp)
                    .background(brush = brush, shape = RoundedCornerShape(4.dp)))
            }
        }
    }

}

@Preview
@Composable
fun PreviewUserLoadingView() {
    UserLoadingView()
}
