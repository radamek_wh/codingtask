package com.williamhill.gamingusers.ui.view.users

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.williamhill.gamingusers.R
import com.williamhill.gamingusers.ui.view.users.contract.UsersState
import com.williamhill.gamingusers.ui.view.users.model.UserItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UsersView(
    modifier: Modifier = Modifier,
    state: UsersState,
    onClick: (String) -> Unit,
) {
    Scaffold(
        topBar = {
            SmallTopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                }
            )
        },
        content = { innerPadding ->
            LazyColumn(
                contentPadding = innerPadding,
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = modifier.fillMaxHeight(),
            ) {
                items(items = state.users, itemContent = {
                    when (it) {
                        UserItem.Skeleton -> UserLoadingView()
                        is UserItem.User -> UserItem(
                            modifier = Modifier,
                            user = it,
                        ) { onClick(it.id) }
                    }
                })
            }
        }
    )

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun UserItem(
    modifier: Modifier = Modifier,
    user: UserItem.User,
    onClick: () -> Unit,
) {
    Surface(
        tonalElevation = 1.dp,
        shadowElevation = 0.dp,
        shape = MaterialTheme.shapes.small,
        modifier = modifier.fillMaxWidth(),
        onClick = onClick
    ) {
        Column(
            modifier = Modifier.padding(PaddingValues(16.dp))
        ) {
            UserItemHeadline(user = user)

            Spacer(modifier = Modifier.size(8.dp))

            UserItemBody(user = user)
        }
    }
}

@Composable
private fun UserItemHeadline(
    modifier: Modifier = Modifier,
    user: UserItem.User,
) {
    Column(
        modifier = modifier.padding(4.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
            ) {
                Text(
                    text = user.name,
                    style = MaterialTheme.typography.headlineMedium,
                )
                Text(
                    modifier = Modifier.padding(top = 4.dp),
                    text = " (${user.age})",
                    style = MaterialTheme.typography.titleLarge
                        .copy(color = MaterialTheme.colorScheme.primary.copy(alpha = 0.4f)),
                )
            }
            ActiveIcon(
                modifier = Modifier.padding(horizontal = 12.dp),
                color = if (user.isActive) Color.Green else Color.Red,
            )
        }
        Spacer(modifier = Modifier.size(4.dp))
        Text(
            text = user.balance,
            style = MaterialTheme.typography.headlineSmall,
        )
    }
}

@Composable
private fun UserItemBody(
    modifier: Modifier = Modifier,
    user: UserItem.User,
) {
    Column(
        modifier = modifier.padding(4.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
    ) {
        Row {
            Icon(
                Icons.Rounded.Email,
                contentDescription = "Email",
                tint = MaterialTheme.colorScheme.primary.copy(alpha = 0.2f),
            )
            Spacer(modifier = Modifier.size(16.dp))
            BodyText(text = user.email)
        }
        Spacer(modifier = Modifier.size(8.dp))
        Row {
            Icon(
                Icons.Rounded.Phone,
                contentDescription = "Phone",
                tint = MaterialTheme.colorScheme.primary.copy(alpha = 0.2f),
            )
            Spacer(modifier = Modifier.size(16.dp))
            BodyText(text = user.phone)
        }
    }
}

@Composable
private fun BodyText(
    text: String,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleMedium
            .copy(color = MaterialTheme.colorScheme.primary.copy(alpha = 0.6f)),
    )
}

@Composable
private fun ActiveIcon(
    modifier: Modifier = Modifier,
    size: Int = 8,
    color: Color = Color.Red,
) {
    Column(modifier = modifier.wrapContentSize(Alignment.Center)) {
        Box(
            modifier = Modifier
                .size(size.dp)
                .clip(CircleShape)
                .background(color)
        )
    } 
}

@Preview
@Composable
fun PreviewUserItemHeader() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItemHeadline(user = user)
}

@Preview
@Composable
fun PreviewUserItemBody() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItemBody(user = user)
}

@Preview
@Composable
fun PreviewUserItem() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItem(user = user) { }
}

@Preview
@Composable
fun PreviewUsers() {
    val users = listOf(
        UserItem.User(
            id = "user1",
            name = "Stephens Church",
            balance = "$1,817.19",
            email = "stephens.church@williamhill.com",
            age = 24,
            phone = "+1 (954) 549-2384",
            isActive = true,
        ),
        UserItem.User(
            id = "user2",
            name = "Cline Palmer",
            balance = "$3,406.68",
            email = "clinepalmer@enomen.com",
            age = 24,
            phone = "+1 (954) 428-3777",
            isActive = false,
        ),
        UserItem.User(
            id = "user3",
            name = "Shauna Miles",
            balance = "$1,284.17",
            email = "shaunamiles@extremo.com",
            age = 39,
            phone = "+1 (954) 449-3646",
            isActive = true,
        ),
    )

    UsersView(state = UsersState(users = users)) { }
}

@Preview
@Composable
fun PreviewUsersLoading() {
    val users = arrayListOf<UserItem.Skeleton>().apply{
        for (i in 0..5) add(UserItem.Skeleton)
    }

    UsersView(state = UsersState(users = users)) { }
}
