package com.williamhill.gamingusers.ui.view.users.contract

import com.williamhill.gamingusers.core.base.BaseViewState
import com.williamhill.gamingusers.ui.view.users.model.UserItem

data class UsersState(
    val users: List<UserItem> = listOf(),
    val errorMessage: String = "",
) : BaseViewState
