package com.williamhill.gamingusers.ui.custom

import androidx.compose.animation.core.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

@Composable
fun loadingShimmerEffect(
    duration: Int = 2000,
    baseColor: Color = MaterialTheme.colorScheme.primary,
): Brush {

    val gradient = listOf(
        baseColor.copy(alpha = 0.16f), //darker grey (80% opacity)
        baseColor.copy(alpha = 0.3f), //lighter grey (35% opacity)
        baseColor.copy(alpha = 0.16f)
    )

    // animate infinite times
    val transition = rememberInfiniteTransition()

    //animate the transition
    val translateAnimation = transition.animateFloat(
        initialValue = 0f,
        targetValue = 2000f,
        animationSpec = infiniteRepeatable(
            animation = tween(
                durationMillis = duration,
                easing = FastOutLinearInEasing
            )
        )
    )

    return Brush.linearGradient(
        colors = gradient,
        start = Offset(250f, 250f),
        end = Offset(
            x = translateAnimation.value,
            y = translateAnimation.value
        )
    )
}
