package com.williamhill.gamingusers.ui.view.users.contract

import com.williamhill.gamingusers.core.base.BaseIntent

sealed class UsersIntent : BaseIntent {
    object LoadUsers: UsersIntent()
    data class NavigateToUser(val id: String): UsersIntent()
}
