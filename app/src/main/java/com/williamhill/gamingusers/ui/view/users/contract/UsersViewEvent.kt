package com.williamhill.gamingusers.ui.view.users.contract

import com.williamhill.gamingusers.core.base.BaseViewEvent

sealed class UsersViewEvent : BaseViewEvent {
    data class NavigateToUser(val id: String): UsersViewEvent()
}
