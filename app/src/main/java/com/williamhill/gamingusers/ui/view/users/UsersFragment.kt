package com.williamhill.gamingusers.ui.view.users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.viewModels
import com.williamhill.gamingusers.core.base.BaseFragment
import com.williamhill.gamingusers.databinding.FragmentUsersBinding
import com.williamhill.gamingusers.ui.theme.UnityTheme
import com.williamhill.gamingusers.ui.view.users.contract.UsersIntent
import com.williamhill.gamingusers.ui.view.users.contract.UsersState
import com.williamhill.gamingusers.ui.view.users.contract.UsersViewEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.merge

@OptIn(FlowPreview::class)
@AndroidEntryPoint
class UsersFragment : BaseFragment<FragmentUsersBinding, UsersState, UsersIntent, UsersViewEvent>() {

    override val viewModel: UsersViewModel by viewModels()

    override val bindLayout: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUsersBinding
        get() = FragmentUsersBinding::inflate

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return setUsersView(viewModel.initialState)
    }

    /**
     * Renders the view with the new state.
     * This must be the only place where the view is changed.
     */
    override fun render(state: UsersState) {
        setUsersView(state)
    }

    /**
     * Initial intents of type [UsersIntent] handled by the VM
     */
    override fun intents(): Flow<UsersIntent> = merge(
        flowOf(UsersIntent.LoadUsers)
    )

    /**
     * Handles events of type [UsersViewEvent] sent from the VM to the view
     */
    override fun handleViewEvent(event: UsersViewEvent) {
        /** TODO: Handle view event **/
    }

    private fun setUsersView(state: UsersState): View =
        binding.composeView.apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                UnityTheme {
                    UsersView(state = state) { }
                }
            }
        }


}