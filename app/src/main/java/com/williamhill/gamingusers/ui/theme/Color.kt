package com.williamhill.unitygaming.ui.theme

import androidx.compose.ui.graphics.Color

val blueGray = Color(0xFF2E4068)
val blue = Color(0xFF00143C)
val blueDark = Color(0xFF01153C)
val blueDarker = Color(0xFF000C24)
val blueBlack = Color(0xFF000714)

val red = Color(0xFFF23C3E)

val yellowLight = Color(0xFFFCFF33)
val yellow = Color(0xFFFAFE04)
val yellowDark = Color(0xFFD8DB00)


