package com.williamhill.gamingusers.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.flow.*

abstract class BaseFragment<VB : ViewBinding, VS : BaseViewState, VI : BaseIntent, VE : BaseViewEvent> : Fragment() {

    abstract val viewModel: BaseViewModel<VS, VI, VE>

    private var _binding: VB? = null
    abstract val bindLayout: (LayoutInflater, ViewGroup?, Boolean) -> VB

    protected val binding: VB
        get() = requireNotNull(_binding)

    protected val sharedViewIntentFlow: MutableSharedFlow<VI> = MutableSharedFlow()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = bindLayout.invoke(inflater, container, false)
        return requireNotNull(_binding).root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate()

        lifecycleScope.launchWhenStarted {
            viewModel
                .viewStateFlow
                .onEach { render(it) }
                .collect()
        }

        lifecycleScope.launchWhenStarted {
            viewModel
                .viewEventFlow
                .onEach { handleViewEvent(it) }
                .collect()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        intents()
            ?.onEach { viewModel.processIntent(it) }
            ?.launchIn(lifecycleScope)
    }

    protected open fun intents(): Flow<VI>? = null

    protected open fun handleViewEvent(event: VE) {}

    protected open fun render(state: VS) {}

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}
