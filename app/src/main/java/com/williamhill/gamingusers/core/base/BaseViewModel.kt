package com.williamhill.gamingusers.core.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@OptIn(FlowPreview::class)
abstract class BaseViewModel<VS : BaseViewState, VI : BaseIntent, VE : BaseViewEvent> : ViewModel() {

    abstract val initialState: VS

    lateinit var viewStateFlow: MutableStateFlow<VS>
        private set

    private val _viewEventFlow = MutableSharedFlow<VE>()
    val viewEventFlow: SharedFlow<VE> = _viewEventFlow

    private val intentFlow = MutableSharedFlow<VI>()

    /**
     * Send intent to change the state of the view
     * @property intent the intent to be sent to the VM
     */
    fun processIntent(intent: VI) {
        viewModelScope.launch {
            intentFlow.emit(intent)
        }
    }

    /**
     * Handles the intent sent from the view to the VM and produces a new [BaseViewState]
     * @property state current state of the view
     * @property intent the intent sent from the view
     *
     * @return new [BaseViewState] after the changes produced by the intent
     */
    protected open fun handleIntent(state: VS, intent: VI): Flow<VS> = flowOf()

    /**
     * Handles the intent sent form the view to the VM and produces a new [BaseViewEvent]
     * @property intent the intent sent from the view
     *
     * @return new [BaseViewEvent] to send an event to the view
     */
    protected open fun handleViewEvent(intent: VI): VE? = null

    open fun onCreate() {
        viewStateFlow = MutableStateFlow(initialState)
        intentFlow
            .onEach { handleViewEvent(it)?.let{ event -> _viewEventFlow.emit(event) } }
            .flatMapConcat { handleIntent(viewStateFlow.value, it) }
            .onEach {
                viewStateFlow.value = it
            }
            .launchIn(viewModelScope)
    }

    protected open fun transformError(error: Throwable): String? {
        /*Implement your error handling*/
        return error.message
    }
}